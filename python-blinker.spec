Name:           python-blinker
Version:        1.4
Release:        6
Summary:        A powerful signal lib based on Python
License:        MIT
URL:            https://pythonhosted.org/blinker/
Source0:        https://pypi.python.org/packages/source/b/blinker/blinker-%{version}.tar.gz

BuildRequires:  python3-devel python3-setuptools

BuildArch:      noarch

%description
Blinker is a signal lib based on Python.It supports both object-to-object
and multicasting for python object.The core of blinker is minimal but provides
quite powerful features.

%package   -n   python3-blinker
Summary:        A powerful signal lib based on Python3
%{?python_provide:%python_provide python3-blinker}

%description -n python3-blinker
Blinker is a signal lib based on Python.It supports both object-to-object
and multicasting for python object.The core of blinker is minimal but provides
quite powerful features.

%prep
%autosetup -n blinker-%{version} -p1

rm -rf %{py3dir}
cp -a . %{py3dir}

%build
cd %{py3dir}
%{__python3} setup.py build
cd -

%install
cd %{py3dir}
%{__python3} setup.py install -O1 --root $RPM_BUILD_ROOT --skip-build
cd -

%files -n python3-blinker
%doc docs/ CHANGES LICENSE README.md PKG-INFO
%{python3_sitelib}/*.egg-info
%{python3_sitelib}/blinker

%changelog
* Wed Oct 21 2020 chengzihan <chengzihan2@huawei.com> - 1.4-6
- Remove subpackage python2-blinker

* Tue Sep 29 2020 liuweibo <liuweibo10@huawei.com> - 1.4-5
- Fix Source0

* Tue Nov 26 2019 zhouyihang <zhouyihang1@huawei.com> - 1.4-4
- Package init
